#!/bin/bash

# Help command for script usage
Help()
{
   echo "Archives files in a provided directory that are older than a specified time."
   echo " - Please note that this script does not support files or directories with spaces"
   echo
   echo "Syntax: archive-log-files.sh -s <source-directory> -d <destination-directory> -t <time-period> [-h|v|m|e]"
   echo "options:"
   echo "h     Print this help"
   echo "v     Add verbosity"
   echo "s     Full path of source directory to archive"
   echo "d     Full path of destination directory to store archived files"
   echo "t     Integer for time period to calculate which files will be archived (e.g. 5, 10, 20)"
   echo "m     Optional time period metric to use to calculate time ago. Defaults to day. Available options: months, days, hours, minutes"
   echo "e     Optional file extension. E.g. log, conf, yaml"
   echo
   echo "Usage:"
   echo "Archive files older than 5 days:"
   echo "archive-log-files.sh -s /var/log/files -d /var/archive -t 5"
   echo
   echo "Archive files older than 10 minutes:" 
   echo "archive-log-files.sh -s /var/log/files -d /var/archive -t 10 -m minutes -v"
   echo
   echo "Archive files older than 10 minutes with file extension of .log:"
   echo "archive-log-files.sh -s /var/log/files -d /var/archive -t 10 -m minutes -e log -v"
}

# Default values
TIME_PERIOD_METRIC=d
VERBOSE=0
FILE_EXTENSION="*"

# Parse our arguments
while getopts ":hs:d:t:m:ve:" option; do
   case $option in
      h) Help
         exit;;
      v) VERBOSE=1;;
      s) SRC_DIR=$OPTARG;;
      d) DESTINATION_DIR=$OPTARG;;
      t) TIME_PERIOD=$OPTARG;;
      e) FILE_EXTENSION=$OPTARG;;
      m) 
         case $OPTARG in
            months)
               TIME_PERIOD_METRIC=m;;
            days)
               TIME_PERIOD_METRIC=d;;
            hours)
               TIME_PERIOD_METRIC=H;;
            minutes)
               TIME_PERIOD_METRIC=M;;
            *)
               echo "Invalid time period metric. Available options: months, days, hours, minutes"
               exit 1;;
         esac
   esac
done

if [ -z "$SRC_DIR" ] || [ -z "$DESTINATION_DIR" ] || [ -z "$TIME_PERIOD" ]; then
   echo "Error: Invalid arguments"
   echo "Use -h for help"
   exit 1
fi

# Calculate the date from the older than length and metric (date where M minus N)
OLDEN_THAN_DATE=$(date -j -v-${TIME_PERIOD}${TIME_PERIOD_METRIC} +"%Y-%m-%d %H:%M:%S")

if [ $VERBOSE == 1 ]; then
   echo "Archiving files before date: $OLDEN_THAN_DATE"
fi

# Get the list of log files in source folder that are after our calculated date
FILES=$(find "$SRC_DIR" -type f -name "*.$FILE_EXTENSION" ! -newermt "$OLDEN_THAN_DATE")
TOTAL_FILES=$(find "$SRC_DIR" -type f -name "*.$FILE_EXTENSION" ! -newermt "$OLDEN_THAN_DATE" | wc -l | xargs)

# Exit if no files to archive
if [ $TOTAL_FILES == 0 ]; then
    echo "No files to archive"
    exit 0;
fi

echo "Archiving $TOTAL_FILES files"

if [ $VERBOSE == 1 ]; then
   echo "Files to archive:"
   echo $FILES | tr " " "\n"
fi

# Loop through each file
COUNT=1
for FILE in $FILES; do

   echo "$COUNT of $TOTAL_FILES"
   echo "   Archiving: $FILE"

   # Remove path and extension from the file to get our filename
   FILENAME=$(basename -- "$FILE" ".${FILE##*.}")

   # Using timestamp in filename to ensure unique filenames and give additional verbosity
   TIMESTAMP=$(date +"%Y%m%d%H%M")

   # Compress file to archive destination folder
   zip -q "$DESTINATION_DIR/$FILENAME-$TIMESTAMP.zip" "$FILE"

   # Remove original log file
   rm -rf "$FILE"

   COUNT=$((COUNT+1))
done;

echo "Archiving complete"