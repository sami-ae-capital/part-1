Challenge: 

- Write a bash script designed to run daily that zips log files older than N days and moves them to an archive folder. 
- The source folder, archive folder and number of days should be parameters passed to the script. 
- You may choose to load these from a configuration file rather than pass directly on the command line.

# Archive Log Files

This script is used to zip and archive files older than a certain time frame. Both the duration and metric of the time frame can be specified as well as other options.

See below for specific command functionality or use the `./archive-log-files.sh -h` to view the help message in terminal.
```
Archives files in a provided directory that are older than a specified time.
 - Please note that this script does not support files or directories with spaces

Syntax: archive-log-files.sh -s <source-directory> -d <destination-directory> -t <time-period> [-h|v|m|e]
options:
h     Print this help
v     Add verbosity
s     Full path of source directory to archive
d     Full path of destination directory to store archived files
t     Integer for time period to calculate which files will be archived (e.g. 5, 10, 20)
m     Optional time period metric to use to calculate time ago. Defaults to day. Available options: months, days, hours, minutes
e     Optional file extension. E.g. log, conf, yaml

Usage:
Archive files older than 5 days:
archive-log-files.sh -s /var/log/files -d /var/archive -t 5

Archive files older than 10 minutes: 
archive-log-files.sh -s /var/log/files -d /var/archive -t 10 -m minutes -v

Archive files older than 10 minutes with file extension of .log:
archive-log-files.sh -s /var/log/files -d /var/archive -t 10 -m minutes -e log -v
```

### File and Directory Names

This script does not support the use of filenames or directories that contain spaces at the moment, though support for this can be added.